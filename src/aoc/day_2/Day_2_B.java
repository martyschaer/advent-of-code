package aoc.day_2;

import aoc.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_2_B {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_2");
        for(int i = 0; i < input.size(); i++){
            for(int j = i + 1; j < input.size(); j++){
                String a = input.get(i);
                String b = input.get(j);
                int diff = diff(a, b);
                if(Integer.bitCount(diff) == 1){
                    System.out.printf("i=%d, j=%d, a=%s, b=%s, diff=%s%n", i, j, a, b, Integer.toBinaryString(diff));
                }
            }
        }
    }

    /**
     * int that has a 1 bit at every difference
     */
    private static int diff(String a, String b){
        if(a.length() != b.length()){
            throw new IllegalArgumentException("Strings must be of same length");
        }

        byte[] aBytes = a.getBytes();
        byte[] bBytes = b.getBytes();

        int diff = 0;
        for(int i = 0; i < a.length(); i++){
            if((((int)aBytes[i]) ^ ((int)bBytes[i])) == 0){
                diff &= 0xFFFFFFFE;
            }else{
                diff |= 1;
            }
            diff <<= 1;
        }
        return diff;
    }
}
