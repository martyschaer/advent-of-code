package aoc.day_2;

import aoc.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_2_A {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_2");
        System.out.printf("Solution = %d", countLetterOccurencesNTimes(input, 2) * countLetterOccurencesNTimes(input, 3));
    }

    /**
     * Here be stream dragons
     */
    private static long countLetterOccurencesNTimes(List<String> input, long occurenceTarget){
        return input.stream()
                .map(boxID -> Arrays.stream(boxID.split("")))
                .map(splitBoxID -> splitBoxID.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())))
                .filter(boxIDCharCounts -> boxIDCharCounts.values().contains(occurenceTarget))
                .count();
    }
}
