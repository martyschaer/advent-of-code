package aoc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {
    private static final String resPath = "/home/marius/programming/aoc/res/";


    public static List<String> readFile(String filename){
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(resPath + filename))) {

            return stream.collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
