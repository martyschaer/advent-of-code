package aoc.day_3;

import aoc.Utils;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day_3_B {
    private static final Pattern CLAIM_PATTERN = Pattern.compile("#(?<id>\\d+)\\s@\\s(?<x>\\d+),(?<y>\\d+):\\s(?<width>\\d+)x(?<height>\\d+)");

    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_3");

        Map<Long, Set<Point>> claims = input.stream()
                .map(Day_3_B::claimToPoints)
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));

        List<Long> ids = new ArrayList<>(claims.keySet());
        List<Set<Point>> claimsPoints = new ArrayList<>(claims.values());
        List<Set<Point>> compareAgainst = copy(claims.values());
        int length = ids.size();
        for(int i = 0; i < length; i++){
            Set<Point> points = claimsPoints.get(i);
            boolean changed = false;
            for(int k = 0; k < length; k++){
                if(k == i){
                    continue;
                }
                changed |= points.removeAll(compareAgainst.get(k));
                if(changed){
                    break;
                }
            }
            if(!changed){
                System.out.printf("Solution = %d%n", ids.get(i));
            }
        }
    }

    private static List<Set<Point>> copy(Collection<Set<Point>> in){
        List<Set<Point>> out = new ArrayList<>(in.size());
        for(Set<Point> set : in){
            out.add(new HashSet<>(set));
        }
        return out;
    }

    private static Map.Entry<Long, Set<Point>> claimToPoints(String claim) {
        Matcher matcher = CLAIM_PATTERN.matcher(claim);
        Set<Point> points = new HashSet<>();
        Long id = 0L;

        if (matcher.find()) {
            id = Long.parseLong(matcher.group("id"));
            int claimX = Integer.parseInt(matcher.group("x"));
            int claimY = Integer.parseInt(matcher.group("y"));
            int width = Integer.parseInt(matcher.group("width"));
            int height = Integer.parseInt(matcher.group("height"));


            for (int x = claimX; x < claimX + width; x++) {
                for (int y = claimY; y < claimY + height; y++) {
                    points.add(new Point(x, y));
                }
            }
        }
        return new AbstractMap.SimpleEntry<>(id, points);
    }
}
