package aoc.day_3;

import aoc.Utils;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day_3_A {
    private static final Pattern CLAIM_PATTERN = Pattern.compile("#\\d+\\s@\\s(?<x>\\d+),(?<y>\\d+):\\s(?<width>\\d+)x(?<height>\\d+)");

    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_3");
        long shared = input.stream()
                .map(Day_3_A::claimToPoints)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() >= 2)
                .count();
        System.out.printf("Solution = %d%n", shared);
    }

    private static Set<Point> claimToPoints(String claim) {
        Matcher matcher = CLAIM_PATTERN.matcher(claim);
        Set<Point> points = new HashSet<>();

        if (matcher.find()) {
            int claimX = Integer.parseInt(matcher.group("x"));
            int claimY = Integer.parseInt(matcher.group("y"));
            int width = Integer.parseInt(matcher.group("width"));
            int height = Integer.parseInt(matcher.group("height"));


            for (int x = claimX; x < claimX + width; x++) {
                for (int y = claimY; y < claimY + height; y++) {
                    points.add(new Point(x, y));
                }
            }
        }
        return points;
    }
}
