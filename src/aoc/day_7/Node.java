package aoc.day_7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Node implements Comparable{
    private String name;
    private List<Node> parents = null;
    private Set<Node> tmpParents = new HashSet<>();

    public int getDuration(){
        return this.name.charAt(0) - 4; // convert name char to ascii
    }

    public Node(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void addParent(Node parent){
        this.tmpParents.add(parent);
    }

    public List<Node> getParents(){
        if(this.parents == null){
            this.parents = new ArrayList<>(tmpParents);
            this.parents.sort(Node::compareTo);
        }
        return this.parents;
    }

    public void removeParentIfPresent(Node parent){
        this.parents.remove(parent);
    }

    public boolean hasNoParents(){
        return this.getParents().isEmpty();
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Node){
            Node other = (Node)o;
            int comp = Integer.compare(this.getParents().size(), other.getParents().size());
            return comp != 0 ? comp : this.name.compareTo(other.name);
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return name != null ? name.equals(node.name) : node.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString(){
        return String.format("[%s] ---> %s", this.getParents().stream().map(Node::getName).collect(Collectors.joining(",")), this.name);
    }
}
