package aoc.day_7;

import aoc.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Day_7_B {
    private static Map<String, Node> nodes = new HashMap<>();
    private static LinkedList<Node> ordered = new LinkedList<>();

    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_7");

        /*
          -->A--->B--
         /    \      \
        C      -->D----->E
         \           /
          ---->F-----
        */
        /*

        input.clear();
        input.add("Step C must be finished before step A can begin.");
        input.add("Step C must be finished before step F can begin.");
        input.add("Step A must be finished before step B can begin.");
        input.add("Step A must be finished before step D can begin.");
        input.add("Step B must be finished before step E can begin.");
        input.add("Step D must be finished before step E can begin.");
        input.add("Step F must be finished before step E can begin.");
        //*/

        long start = System.currentTimeMillis();

        // build graph
        for (String line : input) {
            String[] fields = line.split(" ");
            String p = fields[1];
            String c = fields[7];
            Node parent = nodes.computeIfAbsent(p, Node::new);
            Node child = nodes.computeIfAbsent(c, Node::new);
            child.addParent(parent);
        }

        List<Worker> workers = new ArrayList<>();
        workers.add(new Worker("α"));
        workers.add(new Worker("β"));
        workers.add(new Worker("γ"));
        workers.add(new Worker("δ"));
        workers.add(new Worker("ε"));

        Tasks tasks = new Tasks(nodes.values());

        int duration = 0;

        while(!tasks.done()){
            boolean workBeingDone = false;
            for(Worker worker : workers){
                workBeingDone |= worker.isWorking();
                System.out.println(worker);
                worker.work();

                if(worker.isDone() && worker.getTask() != null){
                    tasks.complete(worker.getTask());
                }

                if(!worker.isWorking()){
                    Node next = tasks.next();
                    if(next != null){
                        worker.startTask(next);
                    }
                }

            }
            if(workBeingDone){
                duration++; // Time passes...
            }
            System.out.printf("%d seconds have passed%n", duration);
        }


        long end = System.currentTimeMillis();

        System.out.printf("Work completed in : %d seconds%n", duration);
        System.out.printf("Completed in %dms%n", end-start);


    }

}
