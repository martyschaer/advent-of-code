package aoc.day_7;

import java.util.*;
import java.util.stream.Collectors;

public class Tasks {
    private List<Node> tasks;
    private List<Node> availableTasks = new LinkedList<>();
    private Set<Node> tasksBeingWorkedOn = new HashSet<>();
    private Set<Node> completed = new HashSet<>();
    private final int count;

    public Tasks(Collection<Node> tbd){
        tasks = new LinkedList<>(tbd);
        count = tbd.size();
        computeAvailableTasks();
    }

    public Node next(){
        if(availableTasks.size() == 0){
            return null;
        }
        Node next = availableTasks.remove(0);
        tasksBeingWorkedOn.add(next);
        return next;
    }

    public void complete(Node task){
        completed.add(task);
        tasksBeingWorkedOn.remove(task);
        removeTaskFromGraph(task);
        computeAvailableTasks();
    }

    private void removeTaskFromGraph(Node finished){
        tasks.remove(finished);
        tasks.forEach(unprocessed -> unprocessed.removeParentIfPresent(finished));
    }

    private void computeAvailableTasks(){
        availableTasks.clear();
        availableTasks.addAll(tasks.stream()
                .filter(Node::hasNoParents)
                .filter(task -> !completed.contains(task))
                .filter(task -> !tasksBeingWorkedOn.contains(task))
                .sorted(Node::compareTo)
                .collect(Collectors.toList()));
    }

    public boolean done(){
        return count == completed.size();
    }
}
