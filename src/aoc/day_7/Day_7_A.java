package aoc.day_7;

import aoc.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Day_7_A {
    private static Map<String, Node> nodes = new HashMap<>();
    private static LinkedList<Node> ordered = new LinkedList<>();

    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_7");

        /*
          -->A--->B--
         /    \      \
        C      -->D----->E
         \           /
          ---->F-----
        */
        /*

        input.clear();
        input.add("Step C must be finished before step A can begin.");
        input.add("Step C must be finished before step F can begin.");
        input.add("Step A must be finished before step B can begin.");
        input.add("Step A must be finished before step D can begin.");
        input.add("Step B must be finished before step E can begin.");
        input.add("Step D must be finished before step E can begin.");
        input.add("Step F must be finished before step E can begin.");
        //*/

        long start = System.currentTimeMillis();

        // build graph
        for (String line : input) {
            String[] fields = line.split(" ");
            String p = fields[1];
            String c = fields[7];
            Node parent = nodes.computeIfAbsent(p, Node::new);
            Node child = nodes.computeIfAbsent(c, Node::new);
            child.addParent(parent);
        }

        List<Node> orphans = calculateOrphans(nodes.values());

        while(!orphans.isEmpty()){
            Node orphan = orphans.get(0);
            ordered.addLast(orphan);
            nodes.values().forEach(node -> node.removeParentIfPresent(orphan));
            orphans = calculateOrphans(nodes.values());
            orphans.removeAll(ordered);
        }

        long end = System.currentTimeMillis();

        System.out.printf("Ordered List: %s%n", ordered.stream().map(Node::getName).collect(Collectors.joining()));
        System.out.printf("Completed in %dms%n", end-start);
    }

    private static List<Node> calculateOrphans(Collection<Node> nodes){
        return nodes.stream() //
                .filter(Node::hasNoParents) //
                .sorted(Node::compareTo) //
                .collect(Collectors.toList()); //
    }

}
