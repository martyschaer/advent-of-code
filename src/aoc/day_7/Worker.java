package aoc.day_7;

public class Worker {
    private Node task;
    private int workLeft;
    private String name;

    public Worker(String name){
        this.name = name;
    }

    public void startTask(Node task){
        this.task = task;
        workLeft = task.getDuration(); // converts ascii name to work duration
    }

    public void work(){
        workLeft--;
    }

    public boolean isDone(){
        return workLeft <= 0;
    }

    public boolean isWorking(){
        return workLeft > 0;
    }

    public Node getTask(){
        return this.task;
    }

    @Override
    public String toString(){
        if(isDone()){
            return String.format("%s is waiting", this.name);
        }
        return String.format("%s is doing %s (%ds left)", this.name, this.task.getName(), this.workLeft);
    }
}
