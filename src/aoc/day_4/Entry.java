package aoc.day_4;

public class Entry {
    public static final String WAKE = "wakes up";
    public static final String SLEEP = "falls asleep";

    private int month;
    private int day;
    private int hour;
    private int minute;
    private String text;
    private int guardID;
    private Guard guard = null;

    public Entry(int month, int day, int hour, int minute, String text) {
        this(month, day, hour, minute, text, -1);
    }

    public Entry(int month, int day, int hour, int minute, String text, int guardID) {
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.text = text;
        this.guardID = guardID;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public String getText() {
        return text;
    }

    public int getGuardID() {
        return guardID;
    }

    public Guard getGuard(){
        if(guard == null){
            guard = new Guard(guardID);
        }
        return guard;
    }

    public boolean isWaking(){
        return text.equals(WAKE);
    }

    public boolean isSleeping(){
        return text.equals(SLEEP);
    }

    public boolean isShift(){
        return !isWaking() && !isSleeping();
    }
}
