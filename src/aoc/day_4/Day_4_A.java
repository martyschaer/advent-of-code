package aoc.day_4;

import aoc.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day_4_A {
    private static final Pattern NOTE_PATTERN = Pattern.compile("\\[(?<datetime>(?<date>\\d{4}-(?<month>\\d{2})-(?<day>\\d{2}))\\s(?<hour>\\d{2}):(?<minute>\\d{2}))\\]\\s(?<note>((.+#(?<guardID>\\d+).+)|.+))");

    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_4");

        // compare only datetime parts
        input.sort(Comparator.comparing(o -> o.substring(0, 18)));

        List<Entry> entries = new ArrayList<>(input.size());

        for (String note : input) {
            Matcher matcher = NOTE_PATTERN.matcher(note);
            if (matcher.find()) {
                int month = Integer.parseInt(matcher.group("month"));
                int day = Integer.parseInt(matcher.group("day"));
                int hour = Integer.parseInt(matcher.group("hour"));
                int minute = Integer.parseInt(matcher.group("minute"));
                String text = matcher.group("note");
                if (text.equals(Entry.WAKE) || text.equals(Entry.SLEEP)) {
                    entries.add(new Entry(month, day, hour, minute, text));
                } else {
                    entries.add(new Entry(month, day, hour, minute, text, Integer.parseInt(matcher.group("guardID"))));
                }
            }
        }

        Map<Integer, Guard> guards = new HashMap<>();

        Iterator<Entry> entryIterator = entries.iterator();

        Guard currentGuard = new Guard(-1);

        while(entryIterator.hasNext()){
            Entry e = entryIterator.next();
            if(e.isShift()){
                currentGuard = guards.computeIfAbsent(e.getGuardID(), Guard::new);
            }else if(e.isSleeping()){
                currentGuard.asleep(e.getHour(), e.getMinute());
            }else if(e.isWaking()){
                currentGuard.awake(e.getHour(), e.getMinute());
            }
        }

        int mostAsleepGuardId = guards.entrySet().stream()
                .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue().minutesAsleep()))
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .findFirst().get().getKey();

        System.out.printf("GuardID = #%d, minute = %d, checksum = %d%n", mostAsleepGuardId,
                guards.get(mostAsleepGuardId).mostAsleepUsuallyAsSolutionFormat(),
                mostAsleepGuardId * guards.get(mostAsleepGuardId).mostAsleepUsuallyAsSolutionFormat());

    }

}
