package aoc.day_4;

import java.time.LocalTime;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Guard {
    private int id;
    private LocalTime fellAsleepTime;
    Map<LocalTime, Integer> record = new HashMap<>();

    public Guard(int id) {
        this.id = id;
    }

    public void asleep(int hour, int minute){
        fellAsleepTime = LocalTime.of(hour, minute);
    }

    public void awake(int hour, int minute){
        LocalTime awakeTime = LocalTime.of(hour, minute);
        LocalTime calcTime = fellAsleepTime;
        while(!calcTime.equals(awakeTime)){
            record.computeIfPresent(calcTime, (k, v) -> v + 1);
            record.putIfAbsent(calcTime, 1);
            calcTime = calcTime.plusMinutes(1L);
        }
    }

    public int minutesAsleep(){
        return this.record.values()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public Map.Entry<LocalTime, Integer> mostAsleepUsually(){
        return this.record.entrySet()
                .stream().max(Map.Entry.comparingByValue()).orElseGet(() -> new AbstractMap.SimpleEntry(LocalTime.MIN, 0));
    }

    public int mostAsleepUsuallyAsSolutionFormat(){
        LocalTime t = mostAsleepUsually().getKey();
        return t.getHour() * 60 + t.getMinute();
    }

    public int getId(){
        return this.id;
    }

}
