package aoc.day_5;

import aoc.Utils;

import java.security.cert.CollectionCertStoreParameters;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day_5_A {

    public static void main(String[] args) throws Exception {
        String inputString = Utils.readFile("input_day_5").get(0); // this input is a single line

        List<Byte> polymer = new LinkedList<>();

        for(Byte b : inputString.getBytes()){
            polymer.add(b);
        }

        System.out.printf("Solution = %d%n", IntStream.rangeClosed(65, 90)
                .mapToObj(i -> (byte) i)
                .map(toRemove -> {
                    byte lower = toRemove;
                    byte upper = (byte) (toRemove + 32);
                    return polymer.stream()
                            .filter(b -> b != lower && b != upper)
                            .collect(Collectors.toCollection(LinkedList::new));
                })
                .map(Day_5_A::react)
                .map(List::size)
                .min(Comparator.naturalOrder()).orElse(-1));
    }

    private static List<Byte> react(List<Byte> polymer){
        System.out.printf("Running len= %d%n", polymer.size());
        boolean changed;
        int lastI = 0;

        do{
            changed = false;
            for(int i = lastI; i < polymer.size() - 1; i++){
                if(Math.abs(polymer.get(i) - polymer.get(i + 1)) == 32){
                    polymer.remove(i);
                    polymer.remove(i);
                    changed = true;
                    lastI = i == 0 ? 0 : i - 1;
                    break;
                }
            }
        }while(changed);

        return polymer;
    }
}
