package aoc.day_8;

import aoc.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_8_A {
    public static void main(String[] args) throws Exception {
        List<String> raw = Utils.readFile("input_day_8");
        List<Integer> input = Arrays.stream(raw.get(0).split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        /*
        2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
        A----------------------------------
            B----------- C-----------
                             D-----
         */
        /*
        input.clear();
        input.addAll(Arrays.asList(2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2));
        //*/

        Parser parser = new Parser(input);
        Node root = parser.parse();

        System.out.printf("Sum of all metadata = %d%n", root.sumMetadataRecursively());
        System.out.printf("The value of the root node = %d%n", root.sumValueRecursively());
    }
}
