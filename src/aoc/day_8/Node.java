package aoc.day_8;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Node {
    List<Node> children = new ArrayList<>();
    List<Integer> metadata = new ArrayList<>();

    public void addChild(Node child){
        this.children.add(child);
    }

    public void addMetaData(List<Integer> metadata){
        this.metadata.addAll(metadata);
    }

    public List<Node> getChildren(){
        return this.children;
    }

    public List<Integer> getMetadata(){
        return this.metadata;
    }

    public Integer sumMetadataRecursively(){
        int sum = 0;
        sum += children.stream()
                .mapToInt(Node::sumMetadataRecursively)
                .sum();
        sum += metadata.stream()
                .mapToInt(Integer::intValue)
                .sum();
        return sum;
    }

    public Integer sumValueRecursively(){
        if(children.size() == 0){
            return metadata.stream()
                    .mapToInt(Integer::intValue)
                    .sum();
        }
        int sum = 0;
        for(Integer meta : metadata){
            if(meta <= children.size()){
                sum += children.get(meta - 1).sumValueRecursively(); // task requires 1-indexation
            }
        }
        return sum;
    }
}
