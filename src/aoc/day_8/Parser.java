package aoc.day_8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Parser {
    private final Iterator<Integer> tokens;

    public Parser(List<Integer> input){
        this.tokens = input.iterator();
    }

    public Node parse() {
        Node node = new Node();
        int childLen = next();
        int metaLen = next();
        for(int i = 0; i < childLen; i++){
            node.addChild(parse());
        }
        node.addMetaData(next(metaLen));

        return node;
    }

    private Integer next(){
        if(tokens.hasNext()){
            return tokens.next();
        }
        return null;
    }

    private List<Integer> next(int amount){
        List<Integer> values = new ArrayList<>(amount);
        for(int i = 0; i < amount; i++){
            if(tokens.hasNext()){
                values.add(tokens.next());
            }
        }
        return values;
    }
}
