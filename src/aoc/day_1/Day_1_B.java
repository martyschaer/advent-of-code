package aoc.day_1;

import aoc.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class Day_1_B {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_1");
        Set<Integer> freqsReached = new HashSet<>();

        Integer sum = 0;

        while(!freqsReached.contains(sum)){
            for(Integer i : input.stream().mapToInt(Integer::parseInt).boxed().collect(Collectors.toList())){
                if(freqsReached.contains(sum)){
                    break;
                }
                freqsReached.add(sum);
                sum += i;
            }
        }
        System.out.printf("Solution = %d", sum);
    }
}
