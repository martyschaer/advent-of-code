package aoc.day_1;

import aoc.Utils;

import java.util.List;

public class Day_1_A {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_1");
        int sum = input.stream()
                .mapToInt(Integer::parseInt)
                .sum();
        System.out.printf("Solution = %d%n", sum);
    }
}
