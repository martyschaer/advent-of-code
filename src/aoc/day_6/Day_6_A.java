package aoc.day_6;

import aoc.Utils;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_6_A {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_6");
        List<Point> points = input.stream()
                .map(line -> {
                    String[] coords = line.split(",");
                    int x = Integer.parseInt(coords[0].trim());
                    int y = Integer.parseInt(coords[1].trim());
                    return new Point(x, y);
                }).collect(Collectors.toList());

        int largestX = points.stream()
                .max(Comparator.comparing(Point::getX))
                .get().x;
        int largestY = points.stream()
                .max(Comparator.comparing(Point::getY))
                .get().y;

        int[][] field = new int[largestY][largestX];


        // populate field initially
        for (int row = 0; row < largestY; row++) {
            field[row] = new int[largestX];
            for (int col = 0; col < largestX; col++) {
                int pt = points.indexOf(getPointWithSmallestDistance(points, row, col));
                field[row][col] = pt;
            }
        }

        // remove areas that touch an edge (considered infinite in size)

        Set<Integer> ids = idsAtEdge(field);

        for (int row = 0; row < largestY; row++) {
            for (int col = 0; col < largestX; col++) {
                int val = field[row][col];
                if(ids.contains(val)){
                    field[row][col] = 0;
                }
            }
        }

        System.out.println("---");

        print(field, largestY, largestX);

        Long size = Arrays.stream(field)
                .flatMap(row -> Arrays.stream(row).boxed())
                .filter(i -> i != 0)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue))
                .get().getValue();

        System.out.printf("Largest non-infinite area has size %d%n", size);

    }

    private static void print(int[][] field, int largestY, int largestX) {
        for (int row = 0; row < largestY; row++) {
            for (int col = 0; col < largestX; col++) {
                System.out.printf("%d,", field[row][col]);
            }
            System.out.print("\n");
        }
    }

    private static Set<Integer> idsAtEdge(int[][] field) {
        Set<Integer> ids = new HashSet<>();

        int maxRow = field.length - 1;
        int maxCol = field[0].length - 1;

        for(int row = 0; row <= maxRow; row++){
            ids.add(field[row][0]);
            ids.add(field[row][maxCol]);
        }

        for(int col = 0; col <= maxCol; col++){
            ids.add(field[0][col]);
            ids.add(field[maxRow][col]);
        }

        return ids;
    }

    private static Point getPointWithSmallestDistance(List<Point> points, int row, int col) {
        List<AbstractMap.SimpleEntry<Point, Integer>> pointDistances = points.stream() // taxi distance (Manhattan geometry)
                .map(p -> new AbstractMap.SimpleEntry<>(p, calculateDistance(p, col, row)))
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .collect(Collectors.toList());
        int shortestDistance = pointDistances.get(0).getValue();
        Point closestPoint = pointDistances.get(0).getKey();
        long equidistantCount = pointDistances.stream()
                .takeWhile(p -> p.getValue().equals(shortestDistance))
                .count();

        return equidistantCount == 1 ? closestPoint : new Point(-1, -1);
    }

    private static Integer calculateDistance(Point p, int col, int row) {
        return Math.abs(p.x - col) + Math.abs(p.y - row);
    }
}
