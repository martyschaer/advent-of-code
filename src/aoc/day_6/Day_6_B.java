package aoc.day_6;

import aoc.Utils;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_6_B {
    public static void main(String[] args) throws Exception {
        List<String> input = Utils.readFile("input_day_6");
        List<Point> points = input.stream()
                .map(line -> {
                    String[] coords = line.split(",");
                    int x = Integer.parseInt(coords[0].trim());
                    int y = Integer.parseInt(coords[1].trim());
                    return new Point(x, y);
                }).collect(Collectors.toList());

        int largestX = points.stream()
                .max(Comparator.comparing(Point::getX))
                .get().x;
        int largestY = points.stream()
                .max(Comparator.comparing(Point::getY))
                .get().y;

        int[][] field = new int[largestY][largestX];


        // populate field initially
        for (int row = 0; row < largestY; row++) {
            field[row] = new int[largestX];
            for (int col = 0; col < largestX; col++) {
                field[row][col] = combinedDistance(points, row, col);
            }
        }

        int threshold = 10_000;

        Long size = Arrays.stream(field)
                .flatMap(row -> Arrays.stream(row).boxed())
                .filter(i -> i < threshold)
                .count();

        System.out.printf("Common area has a size of %d%n", size);

    }

    private static void print(int[][] field, int largestY, int largestX) {
        for (int row = 0; row < largestY; row++) {
            for (int col = 0; col < largestX; col++) {
                System.out.printf("%d,", field[row][col]);
            }
            System.out.print("\n");
        }
    }

    private static int combinedDistance(List<Point> points, int row, int col){
        return points.stream()
                .mapToInt(p -> calculateDistance(p, col, row))
                .sum();
    }

    private static Integer calculateDistance(Point p, int col, int row) {
        return Math.abs(p.x - col) + Math.abs(p.y - row);
    }
}
